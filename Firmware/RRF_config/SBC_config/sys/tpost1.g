; tpost1.g
; called after tool 1 has been selected
; Tool 1 = BMG + Dragon
;

M703	; read current filament config.g

; step/mm value for BMG extruder
M92 E423.3225

; set Z probe trigger value, offset and trigger height
; Nozzle too close => decrease Z value
; Nozzle too far => Increase Z value
;G31 P25 X38 Y-41 Z2.1
;G31 P25 X38 Y-41 Z1.95
;G31 P25 X38 Y-41 Z1.85
G31 P25 X38 Y-41 Z2
M557 X40:290 Y10:290 S60                               ; define mesh grid

; Define hotend thermistor (Triangle Lab - 104NT-4-R025H42G)
M308 S1 P"e1temp" Y"thermistor" A"Dragon" T100000 B4267 C7.06e-8 ; configure sensor 1 as thermistor on pin e0temp

; PID settings for Dragon hotend
M307 H1 R2.812 C302.9 D4.10 S1.00 V0.0

; Wait for set temperatures to be reached
M116 P1

