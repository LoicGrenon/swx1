; homeall.g
; called to home all axes
;
; generated by RepRapFirmware Configuration Tool v3.1.9-LPC+3 on Sun Dec 13 2020 21:46:38 GMT+0100 (heure normale d’Europe centrale)
G91                     ; relative positioning
G1 H2 Z5 F1000          ; lift Z relative to current position
G1 H1 X-305 Y-305 F4500 ; move quickly to X and Y axis endstops and stop there (first pass)
G1 H2 X5 Y5 F6000       ; go back a few mm
G1 H1 X-305 Y-305 F360  ; move slowly to X and Y axis endstops once more (second pass)
G90                     ; absolute positioning
G1 X120 Y180 F6000      ; go to middle of the bed
G30                     ; home Z by probing the bed

G29 S1                  ; load the saved height map and activate bed compensation

; Uncomment the following lines to lift Z after probing
;G91                    ; relative positioning
;G1 Z5 F100             ; lift Z relative to current position
;G90                    ; absolute positioning


