; tpost0.g
; called after tool 0 has been selected
; Tool 0 = Stock X1 Titan extruder + Volcano
;

M703	; read current filament config.g

M92 E489.01	; step/mm value for stock extruder

; set Z probe trigger value, offset and trigger height
; Nozzle too close => decrease Z value
; Nozzle too far => Increase Z value
G31 P25 X24 Y-30 Z3.25
M557 X30:290 Y10:290 S60                               ; define mesh grid

; Define hotend thermistor
M308 S1 P"e1temp" Y"thermistor" A"Stock" T100000 B4725 C7.06e-8 ; configure sensor 1 as thermistor on pin e0temp

; PID settings for stock hotend
M307 H1 R2.311 C301.3 D9.68 S1.00 V0.0

; Wait for set temperatures to be reached
M116 P0

